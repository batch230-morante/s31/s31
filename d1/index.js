// use the "require" directive to load node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "http module" let node.js transfer data using Hyper Text Transfer Protocol a.k.a. HTTP
    // It can create an HTTP server that listens to server ports such as...
        // 3000, 4000, 5000, 8000 (usually used for web development)
// The "http module" is a set of individual files that contain code to create a "component" that help establish data transfer between applications
// Clients (devices/browsers) and server (nodeJS/expressJS application) communicate by exchanging individual messages (request/response)
// Request - the messages sent by the client
// Response - the message sent by the server as response
let http = require("http");

// Using the module's createServer() method, we can create an HTTP server that listens to request on a specified port and gives responses back to the client
// createServer() is a method of the http object responsible for creating a server using Node.js
http.createServer(function(request, response){

    // use the writeHead() method to:
    // Set a status code for the response - a 200 means OK
    // Set the content-type of the response - as a plain text message
    response.writeHead(200, {"content-type": "text/plain"});

    // Send the response with the content "Hello World"
    response.end("Hello World");

}).listen(4000);
// A port is a virtual point where network connections start and end.
// The server will be assigned to port 4000 via the listen() method.
    // where the server will listen to any request that is sent to it and will also send the response via this port.
    




// When the server is running, console will print the message:
console.log("Server is running as localhost: 4000");
// The message or outputs from using console.log() will now be displayed to the terminal
 


